module.exports = (sequelize, Sequelize) => {
    const UserMovie = sequelize.define('UserMovie', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER,
            notEmpty: true,
            allowNull: false
        },
        movie_id: {
            type: Sequelize.INTEGER,
            notEmpty: true,
            allowNull: false
        }
    });

    return UserMovie;
};
