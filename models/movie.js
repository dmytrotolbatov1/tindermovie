module.exports = (sequelize, Sequelize) => {
    const Movie = sequelize.define('Movie', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        title: {
            type: Sequelize.STRING,
            unique: true,
            notEmpty: true
        },
        poster: {
            type: Sequelize.STRING,
            notEmpty: true
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            notEmpty: true
        }
    });
    Movie.associate = (models) => {
        Movie.belongsToMany(models.User, {
            through: 'UserMovie',
            as: 'users',
            foreignKey: 'movie_id'
        });
    };

    return Movie;
};
