module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define('User', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        email: {
            type: Sequelize.STRING,
            notEmpty: true,
            allowNull: false
        },
        username: {
            type: Sequelize.STRING,
            notEmpty: true,
            allowNull: false
        },
        phone: {
            type: Sequelize.INTEGER,
            notEmpty: true,
            allowNull: false
        },
        age: {
            type: Sequelize.INTEGER,
            notEmpty: true,
            allowNull: false
        },
        gender: {
            type: Sequelize.ENUM,
            values: ['MALE', 'FEMALE'],
            allowNull: false
        },
        city: {
            type: Sequelize.ENUM,
            values: ['ivano-frankovsk','vinnica', 'dnepropetrovsk', 'zhitomir', 'zaporozhe', 'kiev', 'krivojrog', 'lvov', 'odessa', 'poltava', 'sumy', 'kherson', 'cherkassy']
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });
    User.associate = (models) => {
        User.belongsToMany(models.User, {
            through: 'UserFriend',
            as: 'friends',
            foreignKey: 'user_id'
        });
        User.belongsToMany(models.User, {
            through: 'UserFriend',
            as: 'friendRequests',
            foreignKey: 'friend_id'
        });
        User.belongsToMany(models.Movie, {
            through: 'UserMovie',
            as: 'movies',
            foreignKey: 'user_id'
        });
    };

    return User;
};
