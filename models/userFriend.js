module.exports = (sequelize, Sequelize) => {
    const UserFriend = sequelize.define('UserFriend', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER,
            notEmpty: true,
            allowNull: false
        },
        friend_id: {
            type: Sequelize.INTEGER,
            notEmpty: true,
            allowNull: false
        },
        requestStatus: {
            type: Sequelize.ENUM,
            values: ['PENDING', 'REJECTED', 'ACCEPTED']
        }
    });

    return UserFriend;
};
