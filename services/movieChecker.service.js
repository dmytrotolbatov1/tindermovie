const { Op } = Sequelize = require('sequelize');
const MovieScrapper = require('./scrapper.service');
const models = require('./../models');

class MovieChecker {
    async updateMovieDB() {
        try {
            const scrapper = new MovieScrapper();
            const movies = await scrapper.getMovies('cherkassy', 'mx-cher-dnipro'); // assume that this cinema has all common movies
            const titles = movies.map((movie) => movie.title);

            movies.map(async (movie) => {
                await models.Movie.findOrCreate({
                    where: { title:  movie.title},
                    defaults: { poster: movie.poster, isActive: true }
                });
            });

            const oldMovies = await models.Movie.findAll({
                where: {
                    title: {
                        [Op.notIn]: titles
                    }
                }
            });

            oldMovies.map(async (movie) => {
                await movie.update({isActive: false});
            });

            return movies;
        } catch (error) {
            return error
        }
    }
}

module.exports = MovieChecker;
