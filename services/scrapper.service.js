const needle = require('needle');
const cheerio = require('cheerio');

class Scrapper {
    constructor() {
        this.URL = 'https://vkino.ua/ua/cinema';
        this.movies = [];
    }

    getMovies(city, cinema) {
        return new Promise((resolve) => {
            needle.get(`${this.URL}/${city}/${cinema}`, (err, res) => {
                if (err) throw err;

                const $ = cheerio.load(res.body);
                $('.schedule-row').each((i, el) => {
                    const movie = {
                        nearestShows: []
                    };
                    const currentElement = $(el);
                    currentElement.find($('.schedule-frame')).find($('.day-schedule')).each((i, el) => {
                        movie.nearestShows.push($(el).text());
                    });
                    movie.poster = currentElement.find($('.film-logo-holder img')).attr('data-src');
                    movie.title = currentElement.find($('.film-name')).text().trim();
                    this.movies.push(movie);
                });

                resolve(this.movies);
            });
        });
    }
}

module.exports = Scrapper;
