const express = require('express');
const router = express.Router();
const { Op } = Sequelize = require('sequelize');
const models = require('./../models');

/*
    get current user
 */
router.get('/me', (req, res) => {
    try {
        const userObj = {
            id: req.user.id,
            email: req.user.email,
            username: req.user.username,
            phone: req.user.phone,
            age: req.user.age,
            gender: req.user.gender,
            city: req.user.city
        };

        return res.send(userObj);
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    get all users
 */
router.get('/', async (req, res) => {
    try {
        const users = await models.User.findAll({
            attributes: ['id', 'email', 'username', 'phone', 'gender', 'age', 'city'],
            include: [{
                model: models.Movie,
                as: 'movies',
                attributes: ['id', 'title', 'poster'],
                required: false,
                where: {
                    isActive: {
                        [Op.not]: false
                    }
                }
            }, {
                model: models.User,
                as: 'friends',
                attributes: ['id', 'email', 'username', 'phone', 'gender', 'age', 'city']
            }]
        });

        return res.status(200).send(users);
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    get user by id
 */
router.get('/:id', async (req, res) => {
    try {
        const user = await models.User.findById(req.params.id, {
            attributes: ['id', 'email', 'username', 'phone', 'gender', 'age', 'city'],
            include: [{
                model: models.Movie,
                as: 'movies',
                attributes: ['id', 'title', 'poster'],
                required: false,
                where: {
                    isActive: {
                        [Op.not]: false
                    }
                }
            }, {
                model: models.User,
                as: 'friends',
                attributes: ['id', 'email', 'username', 'phone', 'gender', 'age', 'city']
            }, {
                model: models.User,
                as: 'friendRequests',
                attributes: ['id', 'email', 'username', 'phone', 'gender', 'age', 'city']
            }]
        });

        return res.status(200).send(user);
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    update user by id
 */
router.put('/:id', async (req, res) => {
    try {
        await models.User.update(req.body, {where: { id: req.params.id }});

        return res.status(200).send({ message: 'User updated' });
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    reject or accept friend request
 */
router.post('/request', async (req, res) => {
    try {
        const { user_id, friend_id, status } = req.body;
        const user = await models.User.findById(user_id);

        if (status === 'REJECTED') {
            await user.removeFriend(friend_id);

            return res.status(200).send({message: 'Friend request was rejected'});
        } else {
            const userFriend = await models.User.findById(friend_id);
            await userFriend.addFriend(user_id, { through: { requestStatus: 'ACCEPTED' }});

            const friendRelationship = await models.UserFriend.find({
                where: {user_id, friend_id}
            });
            await friendRelationship.update({requestStatus: 'ACCEPTED'});

            return res.status(200).send({message: 'Friend request was accepted'});
        }
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    add friend
 */
router.post('/friend', async (req, res) => {
    try {
        const { user_id, friend_id } = req.body;
        const user = await models.User.findById(user_id);

        await user.addFriend(friend_id, { through: { requestStatus: 'PENDING' }});

        return res.status(200).send({ message: 'Friend added' });
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    remove friend
 */
router.delete('/friend', async (req, res) => {
    try {
        const { user_id, friend_id } = req.body;
        const user = await models.User.findById(user_id);
        const friend = await models.User.findById(friend_id);

        await user.removeFriend(friend_id);
        await friend.removeFriend(user_id);

        return res.status(200).send({ message: 'Friend removed' });
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    add movie to selected
 */
router.post('/movie', async (req, res) => {
    try {
        const { user_id, movie_id } = req.body;
        const user = await models.User.findById(user_id);

        await user.addMovie(movie_id);

        return res.status(200).send({ message: 'Movie added' });
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    remove movie from selected
 */
router.delete('/movie', async (req, res) => {
    try {
        const { user_id, movie_id } = req.body;
        const user = await models.User.findById(user_id);

        await user.removeMovie(movie_id);

        return res.status(200).send({ message: 'Movie removed' });
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

module.exports = router;
