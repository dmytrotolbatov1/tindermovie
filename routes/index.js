const routes = require('express').Router();
const passport = require('passport');
const auth = require('./auth');
const movies = require('./movies');
const users = require('./users');

routes.use('/auth', auth);
routes.use('/users', passport.authenticate('jwt', {session: false}), users);
routes.use('/movies', passport.authenticate('jwt', {session: false}), movies);

module.exports = routes;
