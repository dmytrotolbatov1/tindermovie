const passport = require('passport');
const jwt = require('jsonwebtoken');
const express = require('express');
const router  = express.Router();

const TOKEN_LIFE = 9000;
const REFRESH_TOKEN_LIFE = 86400;

router.post('/signup', async (req, res) => {
    try {
        await passport.authenticate('local-signup',
            (err, user) => {
                if (err) {
                    return res.status(400).json({
                        message: 'Something is not right',
                        err: err
                    });
                }

                const userData = {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    phone: user.phone,
                    gender: user.gender,
                    age: user.age,
                    city: user.city,
                    friends: user.friends,
                    movies: user.movies
                };

                return res.status(200).json({user: userData});
            })(req, res);
    } catch (error) {
        return res.status(500).send({ error, message: 'Something went wrong' });
    }
});

router.post('/signin', async (req, res) => {
    try {
        await passport.authenticate(
            'local-signin',
            {session: false},
            (err, user) => {
                if (err || !user) {
                    return res.status(400).json({
                        message: 'Something is not right',
                        user   : user
                    });
                }

                req.login(user, {session: false}, (err) => {
                    if (err) {
                        res.send(err);
                    }

                    const token = jwt.sign(user, process.env.JWT_SECRET, { expiresIn: TOKEN_LIFE});
                    const refreshToken = jwt.sign(user, process.env.JWT_REFRESH_SECRET, { expiresIn: REFRESH_TOKEN_LIFE});
                    const userData = {
                        id: user.id,
                        username: user.username,
                        email: user.email,
                        phone: user.phone,
                        gender: user.gender,
                        age: user.age,
                        city: user.city,
                        friends: user.friends,
                        movies: user.movies
                    };
                    return res.status(200).json({
                        user: userData,
                        token: `Bearer ${token}`,
                        refreshToken: `Bearer ${refreshToken}`
                    });
                });
            }
        )(req, res);
    } catch (error) {
        return res.status(500).send({ error, message: 'Something went wrong' });
    }
});

module.exports = router;
