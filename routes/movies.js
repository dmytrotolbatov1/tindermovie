const express = require('express');
const { Op } = Sequelize = require('sequelize');
const router  = express.Router();
const MovieScrapper = require('../services/scrapper.service');
const models = require('./../models');
const MovieChecker = require('../services/movieChecker.service');

/*
    get all current movies
 */
router.get('/', async (req, res) => {
    try {
        const scrapper = new MovieScrapper();
        const { city, cinema } = req.query;
        const movies = await scrapper.getMovies(city, cinema);

        return res.status(200).send(movies);
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    update MovieDB
 */
router.get('/updatedb', async (req, res) => {
    try {
        const movieChecker = new MovieChecker();

        const movies = await movieChecker.updateMovieDB();
        return res.status(200).send(movies);
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    get users that have the same selected movies as current user
 */
router.get('/common', async (req, res) => {
    try {
        const userMovies = await models.UserMovie.findAll({where: {user_id: req.user.id}});
        const commonMovieIds = userMovies.map((movie) => movie.movie_id);
        const selectedUsers = await models.User.findAll({
            where: {
                id: {[Op.not]: req.user.id},
                city: req.query.city
            },
            include: [{
                model: models.Movie,
                as: 'movies',
                attributes: ['id', 'title', 'poster'],
                where: {id: {[Op.in]: commonMovieIds}}
            }]
        });

        return res.status(200).send(selectedUsers);
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

/*
    get movies that are selected by users
 */
router.get('/count', async (req, res) => {
    try {
        const { city } = req.query;
        const movies = await models.Movie.findAll({
            attributes: ['id', 'title', 'poster'],
            where: {
                isActive: {
                    [Op.not]: false
                }
            },
            include: [{
                model: models.User,
                as: 'users',
                attributes: ['id', 'email', 'username', 'phone', 'age', 'gender', 'city'],
                where: { city }
            }]
        });

        return res.status(200).send(movies);
    } catch (error) {
        return res.status(500).send({ message: 'Something went wrong' });
    }
});

module.exports = router;
