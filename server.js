require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const passport = require('passport');
const session = require('express-session');
const schedule = require('node-schedule');
const models = require('./models');
const routes = require('./routes');
const MovieChecker = require('./services/movieChecker.service');

const app = express();
const API_PORT = process.env.PORT || 3000;
const movieChecker = new MovieChecker();

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(session({ secret: process.env.JWT_SECRET, resave: true, saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport, models.User);

app.use('/v1', routes);

models.sequelize.sync().then(() => {
    app.listen(API_PORT, () => {
        console.log('Server is listening on port ' + API_PORT);

        schedule.scheduleJob('* 3 * * *', () => {
            movieChecker.updateMovieDB()
                .then((movies) => {
                    console.log(movies, 'MovieDB updated');
                })
                .catch((err) => {
                    console.log(err);
                });
        });
    });
});
