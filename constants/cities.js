const CITIES = [
    {
        city: 'ivano-frankovsk',
        cinemas: ['liniakino-kosmos']
    },
    {
        city: 'vinnica',
        cinemas: ['kocubinsky']
    },
    {
        city: 'dnepropetrovsk',
        cinemas: ['most-kino', 'mx-dnepr-dafi', 'mx-dnepr-karavan']
    },
    {
        city: 'zhitomir',
        cinemas: ['mx-zhitom-global', 'ukraina-zhitomir']
    },
    {
        city: 'zaporozhe',
        cinemas: ['bayda', 'dovgenko-zaporoj', 'mx-zp-avrora']
    },
    {
        city: 'kiev',
        cinemas: ['atmasfera360-museum', 'atmasfera360-podol', 'atmasfera360-park', 'boomer', 'kievrus', 'rossia', 'kiev', 'leyptsig', 'leningrad', 'liniakino-aladdin', 'liniakino-magelan', 'liniakino-metropolis', 'lira', 'magnat', 'mx-kiev-comod', 'mx-kiev-lavinamall', 'mx-kiev-skymall', 'mx-kiev-atmosfera', 'mx-kiev-karavan', 'mx-kiev-prospect', 'kinoodessa-kvadrat', 'oskar', 'gulliver', 'shevchenko']
    },
    {
        city: 'krivojrog',
        cinemas: ['mx-krivrog-union', 'mx-krivrog-victory', 'olymp']
    },
    {
        city: 'lvov',
        cinemas: ['filmcenter', 'kp-lvov', 'kp-kopernik', 'kp-dovjenko', 'mx-lviv-victoriagardens', 'pk-lvov2', 'pk-lvov']
    },
    {
        city: 'odessa',
        cinemas: ['kinoodessa-kinostar', 'kinoodessa-moskva', 'pk-odessa', 'pk-odessa2', 'kinoodessa-rodina']
    },
    {
        city: 'poltava',
        cinemas: ['mx-poltava-ekvator']
    },
    {
        city: 'sumy',
        cinemas: ['pk-sumy']
    },
    {
        city: 'kherson',
        cinemas: ['mx-herson-fab']
    },
    {
        city: 'cherkassy',
        cinemas: ['mx-cher-lubava', 'mx-cher-dnipro', 'ukraina-cherkassy']
    }
];
