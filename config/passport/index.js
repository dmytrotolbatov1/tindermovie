const bCrypt = require('bcrypt-nodejs');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require("passport-jwt");

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

module.exports = (passport, user) => {
    const User = user;

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        User.findById(id).then((user) => {
            if (user) {
                done(null, user.get());
            } else {
                done(user.errors, null);
            }
        });
    });

    passport.use('local-signup', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        (req, email, password, done) => {
            const generateHash = (password) => bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
            User.findOne({
                where: {email}
            }).then((user) => {
                if (user) {
                    return done(null, false, {
                        message: 'That email is already taken'
                    });
                } else {
                    const userPassword = generateHash(password);
                    const data =
                        {
                            email: email,
                            password: userPassword,
                            username: req.body.username,
                            phone: req.body.phone,
                            gender: req.body.gender,
                            city: req.body.city,
                            age: req.body.age
                        };

                    User.create(data).then((newUser) => {
                        if (!newUser) {
                            return done(null, false);
                        }

                        if (newUser) {
                            return done(null, newUser);
                        }
                    });
                }
            })
                .catch((err) => {
                    console.log(err);
                    return done(null, false, { message: 'Something went wrong with your Signup' });
                });
        }
    ));

    passport.use('local-signin', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        (req, email, password, done) => {
            User.findOne({ where: {email}}).then((user) => {

                if (!user) {
                    return done(null, false, { message: 'Email or password is incorrect.' });
                }

                const userinfo = user.get();

                return done(null,userinfo);

            }).catch((err) => {
                console.log("Error:", err);
                return done(null, false, { message: 'Something went wrong with your Signin' });
            });
        }
    ));

    passport.use(new JWTStrategy({
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
            secretOrKey   : process.env.JWT_SECRET
        },
        (jwtPayload, done) => {

            return User.findById(jwtPayload.id)
                .then(user => {
                    return done(null, user);
                })
                .catch(err => {
                    return done(err);
                });
        }
    ));
};
